<?php

use App\Http\Controllers\ClientController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\PointController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/companies', [CompanyController::class, 'getAllCompanys']);

Route::get('/companies/{id}', [CompanyController::class, 'getCompany']);

Route::post('/companies', [CompanyController::class, 'createCompany']);

Route::get('/companies/{id}/clients', [ClientController::class, 'getAllClients']);

Route::get('/companies/{id}/clients/{client_id}', [ClientController::class, 'getClient']);

Route::post('/companies/{id}/clients', [ClientController::class, 'createClient']);

Route::post('/companies/{id}/clients/{client_id}/points', [PointController::class, 'createPoints']);