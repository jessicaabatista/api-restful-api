<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
})->name('home');

Auth::routes();

Route::get('email\NewClient', function () {
    \Illuminate\Support\Facades\Mail::send(new \App\Mail\NewClientNotification());
})->name('emails.NewClient');