<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            Client::class,
            Company::class,
            Card::class,
            Point::class
        ]);
    }
}

class Client extends Seeder
{
    public function run()
    {
        DB::table('client')->insert(array(
            array(
                'fullname' => 'Jéssica Batista',
                'email' => 'jessicaabta@gmail.com',
                'cellphone' => '73981649868',
                'created_at' => date('Y/m/d H:i:s'),
                'updated_at' => date('Y/m/d H:i:s'),
            ),
        ));
    }
}

class Company extends Seeder
{
    public function run()
    {
        DB::table('company')->insert(array(
            array(
                'company_name' => 'ACME Corporation',
                'document' => '66737788000121',
                'created_at' => date('Y/m/d H:i:s'),
                'updated_at' => date('Y/m/d H:i:s'),
            ),
        ));
    }
}

class Card extends Seeder
{
    public function run()
    {
        DB::table('card')->insert(array(
            array(
                'company_id' => 1,
                'client_id' => 1,
                'created_at' => date('Y/m/d H:i:s'),
            ),
        ));
    }
}

class Point extends Seeder
{
    public function run()
    {
        DB::table('point')->insert(array(
            array(
                'card_id' => 1,
                'points' => 0,
                'spent' => 0.00,
                'created_at' => date('Y/m/d H:i:s'),
                'updated_at' => date('Y/m/d H:i:s'),
            ),
        ));
    }
}
