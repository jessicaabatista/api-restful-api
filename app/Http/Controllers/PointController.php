<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PointController extends Controller
{
    public function createPoints(Request $request, $company_id, $client_id)
    {
        if (isset($request['token']) && $request['token'] === env('TOKEN_AUTH')) {

            $select = DB::table('point')
            ->join('card', function ($join) {
                $join->on('point.card_id', '=', 'card.id');
            })
            ->where('card.company_id', $company_id)
            ->where('card.client_id', $client_id)
            ->select('point.*')
            ->first();

            DB::table('point')->where('id', $select->id)
            ->update([
                'points' => $select->points + intval($request->points)
                , 'spent' => $select->spent + (float) str_replace(',', '.', str_replace('.', '', $request->spent))
            ]);

            return response()->json([
                "message" => "Pontos cadastrados!"
            ], 201);
        } else {
            return response()->json([
                "message" => "Acesso negado!"
            ], 201);
        }
    }
}
