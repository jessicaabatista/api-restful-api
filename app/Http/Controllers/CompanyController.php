<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function getAllCompanys(Request $request)
    {
        if (isset($request['token']) && $request['token'] === env('TOKEN_AUTH')) {

            $companys = Company::get()->toJson(JSON_PRETTY_PRINT);
            return response($companys, 200);
        } else {
            return response()->json([
                "message" => "Acesso negado!"
            ], 201);
        }
    }

    public function getCompany(Request $request, $id)
    {
        if (isset($request['token']) && $request['token'] === env('TOKEN_AUTH')) {
            if (Company::where('id', $id)->exists()) {
                $company = Company::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
                return response($company, 200);
            } else {
                return response()->json([
                    "message" => "Registro não encontrado."
                ], 404);
            }
        } else {
            return response()->json([
                "message" => "Acesso negado!"
            ], 201);
        }
    }

    public function createCompany(Request $request)
    {
        if (isset($request['token']) && $request['token'] === env('TOKEN_AUTH')) {
            $company = new Company;
            $company->company_name = $request->company_name;
            $company->document = $request->document;
            $company->created_at = date("Y-m-d H:i:s");
            $company->save();

            return response()->json([
                "message" => "Empresa cadastrada!"
            ], 201);
        } else {
            return response()->json([
                "message" => "Acesso negado!"
            ], 201);
        }
    }
}
