<?php

namespace App\Http\Controllers;

use App\Mail\NewClientNotification;
use App\Models\Client;
use App\Models\Point;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ClientController extends Controller
{
    public function getAllClients(Request $request, $id)
    {
        if (isset($request['token']) && $request['token'] === env('TOKEN_AUTH')) {

            $select_clients = DB::table('client')
                ->join('card', function ($join) {
                    $join->on('client.id', '=', 'card.client_id');
                })
                ->where('card.client_id', $id)
                ->select('client.id');

            $clients = Client::whereIn('id', $select_clients)->get()->toJson(JSON_PRETTY_PRINT);
            return response($clients, 200);
        } else {
            return response()->json([
                "message" => "Acesso negado!"
            ], 201);
        }
    }

    public function getClient(Request $request, $company_id, $client_id)
    {
        if (isset($request['token']) && $request['token'] === env('TOKEN_AUTH')) {

            $select_client = DB::table('client')
                ->join('Card', function ($join) {
                    $join->on('Client.id', '=', 'Card.client_id');
                })
                ->where('Card.company_id', $company_id)
                ->where('Card.client_id', $client_id)
                ->select('Client.id');

            $client = Client::whereIn('id', $select_client)->get()->toJson(JSON_PRETTY_PRINT);
            return response($client, 200);
        } else {
            return response()->json([
                "message" => "Acesso negado!"
            ], 201);
        }
    }

    public function createClient(Request $request, $company_id)
    {
        if (isset($request['token']) && $request['token'] === env('TOKEN_AUTH')) {
            $client = DB::table('client')->insertGetId([
                'fullname' => $request->fullname
                , 'email' => $request->email
                , 'cellphone' => $request->cellphone
                , 'created_at' => date("Y-m-d H:i:s")
            ]);

            $card = DB::table('card')->insertGetId([
                'company_id' => $company_id
                , 'client_id' => $client
                , 'created_at' => date("Y-m-d H:i:s")
            ]);

            $points = new Point();
            $points->card_id = $card;
            $points->points = 0;
            $points->spent = 0;
            $points->created_at = date("Y-m-d H:i:s");
            $points->save();

            $getClient = Client::where('id', $client)->first();

            Mail::to($getClient->email)->send(new NewClientNotification($getClient));

            return response()->json([
                "message" => "Cliente cadastrado!"
            ], 201);
        } else {
            return response()->json([
                "message" => "Acesso negado!"
            ], 201);
        }
    }
}
