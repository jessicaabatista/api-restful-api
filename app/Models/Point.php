<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    use HasFactory;

    public $table = "point";
    protected $primaryKey = "id"; 
    public $timestamps = false;

    public function card()
    {
        return $this->hasOne('App\Models\Card', 'id', 'card_id');
    }
}
